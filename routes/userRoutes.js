const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');


//Checking Email
router.post("/checkEmail", (req,res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController))
});

//Registration

router.post('/register',(req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

//LOGIN
router.post('/login',(req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

//RETRIEVING USER DETAILS

// router.get('/details',(req,res) => {
// 	userController.getProfile(req.params.id).then(resultFromController => res.send(resultFromController))
// });

router.post("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});


router.post("/enroll",auth.verify,(req,res) => {

	let data = {
		userId : req.body.userId,
		courseId: req.body.courseId,
		payload: auth.decode(req.headers.authorization)
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController))
})

module.exports = router;