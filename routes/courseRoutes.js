const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseControllers');
const auth = require('../auth');

// Add a course

router.post('/', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization)
	courseController.addCourse(req.body, userData.isAdmin).then(resultFromController => res.send(resultFromController))
});

//retrieve all courses

router.get('/all',auth.verify,(req,res) =>{

	const data = auth.decode(req.headers.authorization)

	courseController.getAllCourses(data).then(resultFromController => res.send(resultFromController));
});

//retrieval of active courses

router.get('/',(req,res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
})


// retrieval of specific course

router.get('/:courseId', (req,res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
});



//update course

router.put('/:courseId', auth.verify, (req,res) => {

	const data = {
		courseId: req.params.courseId,
		payload: auth.decode(req.headers.authorization),
		updatedCourse : req.body
	}

	courseController.updateCourse(data).then(resultFromController => res.send(resultFromController))
});

//archiving a course

	router.put('/:courseId/archive',auth.verify,(req,res) => {
		const data = {
			courseId: req.params.courseId,
			payload:  auth.decode(req.headers.authorization),
			archiveCourse: req.body
		}
		courseController.archivedCourse(data).then(resultFromController => res.send(resultFromController))
	})


module.exports = router;
